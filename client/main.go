package main

import (
	"context"
	"fmt"
	"os"

	"github.com/pkg/errors"
	"github.com/urfave/cli"
)

var Upload = cli.Command{
	Name:   "upload",
	Usage:  "uploads a file",
	Action: uploadAction,
	Flags: []cli.Flag{
		&cli.StringFlag{
			Name:  "address",
			Usage: "Address of server to connect to",
			Value: "localhost:9090",
		},
		&cli.IntFlag{
			Name:  "chunk-size",
			Usage: "size of the message chunks to stream",
			Value: (1 << 12),
		},
		&cli.StringFlag{
			Name:  "file",
			Usage: "file to upload",
		},
		&cli.StringFlag{
			Name:  "root-certificate",
			Usage: "path to a root certificate to add to the root CAs",
		},
		&cli.BoolFlag{
			Name:  "compress",
			Usage: "whether or not to enable payload compression",
		},
	},
}

func main() {
	app := &cli.App{
		Name:  "gRPC Upload",
		Usage: "upload files to gRPC server",
		Commands: []cli.Command{
			Upload,
		},
		Flags: []cli.Flag{
			&cli.BoolFlag{
				Name:  "debug",
				Usage: "enables debug logging",
			},
		},
	}

	app.Run(os.Args)
}

func uploadAction(c *cli.Context) error {
	var (
		chunkSize       = c.Int("chunk-size")
		address         = c.String("address")
		file            = c.String("file")
		rootCertificate = c.String("root-certificate")
		compress        = c.Bool("compress")
	)

	if address == "" {
		err := errors.New("address must be provided")
		fmt.Println(fmt.Sprintf("Error: %s", err))
		return err
	}

	if file == "" {
		err := errors.New("file must be provided")
		fmt.Println(fmt.Sprintf("Error: %s", err))
		return err
	}

	grpcClient, err := NewClientGRPC(ClientGRPCConfig{
		Address:         address,
		ChunkSize:       chunkSize,
		RootCertificate: rootCertificate,
		Compress:        compress,
	})

	if err != nil {
		return err
	}

	stat, err := grpcClient.UploadFile(context.Background(), file)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	defer grpcClient.Close()

	fmt.Printf("Upload complete in %d ms\n", stat.FinishedAt.Sub(stat.StartedAt).Milliseconds())

	return nil
}
