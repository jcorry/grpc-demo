package main

import (
	"context"
	"io"
	"os"
	"time"

	"github.com/pkg/errors"
	"gitlab.com/jcorry/grpc-demo/service/api"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
	_ "google.golang.org/grpc/encoding/gzip"
)

type Client interface {
	Close()
}

type ClientGRPC struct {
	conn      *grpc.ClientConn
	client    api.PhotoServiceClient
	chunkSize int
}

type ClientGRPCConfig struct {
	Address         string
	ChunkSize       int
	RootCertificate string
	Compress        bool
}

type Stats struct {
	StartedAt  time.Time
	FinishedAt time.Time
}

// NewGRPCClient returns a new ClientGRPC
func NewClientGRPC(cfg ClientGRPCConfig) (*ClientGRPC, error) {
	c := ClientGRPC{}
	var (
		grpcOpts = []grpc.DialOption{}
		err      error
	)

	if cfg.Address == "" {
		return nil, errors.New("address must be specified")
	}

	if cfg.Compress {
		grpcOpts = append(grpcOpts, grpc.WithDefaultCallOptions(grpc.UseCompressor("gzip")))
	}

	if cfg.RootCertificate != "" {
		grpcCreds, err := credentials.NewClientTLSFromFile(cfg.RootCertificate, "")
		if err != nil {
			return nil, errors.Wrapf(err, "failed to create grpc tls client via root cert: %s", cfg.RootCertificate)
		}
		grpcOpts = append(grpcOpts, grpc.WithTransportCredentials(grpcCreds))
	} else {
		grpcOpts = append(grpcOpts, grpc.WithInsecure())
	}

	switch {
	case cfg.ChunkSize == 0:
		return nil, errors.New("ChunkSize must be specified")
	case cfg.ChunkSize > (1 << 22):
		return nil, errors.New("ChunkSize must be < 4MB")
	default:
		c.chunkSize = cfg.ChunkSize
	}

	c.conn, err = grpc.Dial(cfg.Address, grpcOpts...)

	if err != nil {
		return nil, errors.Wrapf(err, "failed to start grpc connection with address: %s", cfg.Address)
	}

	c.client = api.NewPhotoServiceClient(c.conn)

	return &c, nil
}

func (c *ClientGRPC) UploadFile(ctx context.Context, f string) (Stats, error) {
	var (
		writing = true
		buf     []byte
		n       int
		file    *os.File
		status  *api.UploadStatus
		s       Stats
	)

	file, err := os.Open(f)
	if err != nil {
		return s, errors.Wrapf(err, "failed to open file: %s", file)
	}

	stream, err := c.client.Upload(ctx)
	if err != nil {
		return s, errors.Wrapf(err, "failed to create upload stream for file: %s\n%s", f, err)
	}
	defer stream.CloseSend()

	s.StartedAt = time.Now()

	buf = make([]byte, c.chunkSize)

	for writing {
		n, err = file.Read(buf)
		if err != nil {
			if err == io.EOF {
				writing = false
				err = nil
				continue
			}
			return s, errors.Wrapf(err, "errored while copying from file to buf")
		}

		err = stream.Send(&api.FileUpload{
			Metadata: &api.FileMeta{
				Filename:  "",
				Extension: "",
			},
			Content: buf[:n],
		})
		if err != nil {
			return s, errors.Wrapf(err, "failed to send chunk via stream")
		}
	}

	s.FinishedAt = time.Now()

	status, err = stream.CloseAndRecv()
	if err != nil {
		return s, errors.Wrapf(err, "failed to receive upstream status response")
	}

	if status.Code != api.UploadStatusCode_Ok {
		return s, errors.Errorf("upload failed - msg: %s", status.Message)
	}

	return s, nil
}

// Close closes the connection
func (c *ClientGRPC) Close() {
	if c.conn != nil {
		c.conn.Close()
	}
}
