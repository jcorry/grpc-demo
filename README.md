# grpc-demo

## Create a photo

Upload a photo to Cloudinary and store its Exif data in redis

## List photos

Get a list of photos that have been uploaded to the service

## Read a photo

Read a single photo by ID, retreiving all data stored for that photo
