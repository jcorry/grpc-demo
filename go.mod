module gitlab.com/jcorry/grpc-demo

go 1.12

require (
	github.com/ardanlabs/conf v1.1.0
	github.com/go-redis/redis/v7 v7.0.0-beta.4
	github.com/gogo/protobuf v1.3.1
	github.com/golang/protobuf v1.3.2
	github.com/mwitkow/go-proto-validators v0.2.0
	github.com/pkg/errors v0.8.1
	github.com/urfave/cli v1.22.1
	golang.org/x/net v0.0.0-20190827160401-ba9fcec4b297 // indirect
	golang.org/x/sys v0.0.0-20190830142957-1e83adbbebd0 // indirect
	golang.org/x/text v0.3.2 // indirect
	google.golang.org/grpc v1.22.1
)
