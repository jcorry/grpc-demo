SHELL := /bin/bash
.PHONY: compile api clean dep lint coverage coverhtml
export PROJECT = grpc-demo

PROTOC_GEN_GO := $(GOPATH)/bin/protoc-gen-go
PROTOBUF_PATH := $(shell go list -f '{{ .Dir }}' -m github.com/golang/protobuf)
GOGO_PATH := $(shell go list -f '{{ .Dir }}' -m github.com/gogo/protobuf)
VALIDATORS_PATH := $(shell go list -f '{{ .Dir }}' -m github.com/mwitkow/go-proto-validators)
PROJECT_NAME := "grpc-demo"
PKG := "gitlab.com/jcorry/$(PROJECT_NAME)"
PKG_LIST := $(shell go list ./... | grep -v /vendor/)
GO_FILES := $(shell find . -name '*.go' | grep -v /vendor/ | grep -v _test.go)

# If $GOPATH/bin/protoc-gen-go does not exist, we'll run this command to install
# it.
$(PROTOC_GEN_GO):
	go get -u github.com/golang/protobuf/protoc-gen-go

dep:
	go get -u github.com/golang/protobuf
	go get -u github.com/gogo/protobuf@master
	go get -u github.com/mwitkow/go-proto-validators
	go get -u github.com/mwitkow/go-proto-validators/protoc-gen-govalidators

api.pb.go: ./protoc/api.proto | $(PROTOC_GEN_GO)
	protoc -I ./protoc \
		-I $(GOPATH)/src \
		-I $(PROTOBUF_PATH) \
		-I $(GOGO_PATH) \
		-I $(GOGO_PATH)/protobuf \
		-I $(VALIDATORS_PATH) \
		--go_out=plugins=grpc:./service/api \
		--govalidators_out=./service/api/ \
		./protoc/*.proto

# This is a "phony" target - an alias for the above command, so "make compile"
# still works.
compile: api.pb.go

clean:
	docker system prune -f

up:
	docker-compose up

down:
	docker-compose down

deps-reset:
	git checkout -- go.mod
	go mod tidy

deps-upgrade:
	go get $(go list -f '{{if not (or .Main .Indirect)}}{{.Path}}{{end}}' -m all)

deps-cleancache:
	go clean -modcache

api:
	docker build \
		-t grpc-demo-api \
		--build-arg PACKAGE_NAME=api \
		--build-arg VCS_REF=`git rev-parse HEAD` \
		--build-arg BUILD_DATE=`date -u +"%Y-%m-%dT%H:%M:%SZ"` \
		.

	docker-compose up

lint: ## Lint the files
	@golint -set_exit_status ${PKG_LIST}

test: ## Run unittests
	@go test -short ${PKG_LIST}

race: dep ## Run data race detector
	@go test -race -short ${PKG_LIST}

msan: dep ## Run memory sanitizer
	@go test -msan -short ${PKG_LIST}

coverage: ## Generate global code coverage report
	./tools/coverage.sh;

coverhtml: ## Generate global code coverage report in HTML
	./tools/coverage.sh html;
