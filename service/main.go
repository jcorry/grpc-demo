package main

import (
	"expvar"
	"fmt"
	"log"
	"net"
	"os"

	"google.golang.org/grpc/credentials"

	"gitlab.com/jcorry/grpc-demo/service/api"
	"google.golang.org/grpc"

	"github.com/go-redis/redis/v7"

	"github.com/pkg/errors"

	"github.com/ardanlabs/conf"

	_ "google.golang.org/grpc/encoding/gzip"
)

// build is the git version of this program. It is set using build flags in the makefile.
var build = "develop"

// type for Context keys
type contextKey int

const (
	clientIDKey contextKey = iota
)

func main() {
	if err := run(); err != nil {
		log.Println("error :", err)
		os.Exit(1)
	}
}

func run() error {
	// =========================================================================
	// Logging

	log := log.New(os.Stdout, "api : ", log.LstdFlags|log.Lmicroseconds|log.Lshortfile)

	// =========================================================================
	// Configuration

	var cfg struct {
		Redis struct {
			Host string `conf:"default:0.0.0.0:6379"`
		}
		Cloudinary struct {
			APIKey    string `conf:"default:apikey"`
			APISecret string `conf:"default:apisecret"`
			CloudName string `conf:"default:cloudname"`
		}
		Address struct {
			GRPC string `conf:"default:0.0.0.0:9090"`
			Rest string `conf:"default:0.0.0.0:8080"`
		}
	}

	args := append(os.Args[1:], os.Environ()...)
	if err := conf.Parse(args, "api", &cfg); err != nil {
		if err == conf.ErrHelpWanted {
			usage, err := conf.Usage("api", &cfg)
			if err != nil {
				return errors.Wrap(err, "generating config usage")
			}
			fmt.Println(usage)
			return nil
		}
		return errors.Wrap(err, "parsing config")
	}

	// =========================================================================
	// App Starting
	expvar.NewString("build").Set(build)
	log.Printf("main : Started : Application initializing : version %q", build)
	defer log.Println("main : Completed")

	out, err := conf.String(&cfg)
	if err != nil {
		return errors.Wrap(err, "generating config for output")
	}
	log.Printf("main : Config :\n%v\n", out)

	// =========================================================================
	// Redis client
	redisClient := redis.NewClient(&redis.Options{
		Addr: cfg.Redis.Host,
	})

	// =========================================================================
	// Tracing support

	// =========================================================================
	// Debug Service

	// =========================================================================
	// API Starting
	certFile := "certs/server.crt"
	keyFile := "certs/server.key"

	go func() {
		err := startGRPCServer(cfg.Address.GRPC, certFile, keyFile, redisClient)
		if err != nil {
			log.Fatalf("failed to start gRPC server: %s", err)
		}
	}()

	// =========================================================================
	// Shutdown

	select {}

	return nil
}

func startGRPCServer(address, certFile, keyFile string, redisClient *redis.Client) error {
	lis, err := net.Listen("tcp", address)
	if err != nil {
		return fmt.Errorf("failed to listen: %v", err)
	}

	// Get a new server to handle requests
	s := api.NewAPIServer(redisClient)

	creds, err := credentials.NewServerTLSFromFile(certFile, keyFile)
	if err != nil {
		return fmt.Errorf("could not load TLS keys: %s", err)
	}

	opts := []grpc.ServerOption{
		grpc.Creds(creds),
	}

	grpcServer := grpc.NewServer(opts...)

	api.RegisterPhotoServiceServer(grpcServer, &s)

	log.Printf("starting HTTP/2 gRPC server on %s", address)
	if err := grpcServer.Serve(lis); err != nil {
		return fmt.Errorf("failed to serve: %s", err)
	}

	return nil
}
