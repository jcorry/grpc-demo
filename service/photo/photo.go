package photo

import "github.com/go-redis/redis/v7"

type Photo struct {
	redis *redis.Client
}

func NewPhoto(r *redis.Client) *Photo {
	return &Photo{redis: r}
}
