package api

import (
	"context"
	"io"

	"github.com/pkg/errors"

	"github.com/go-redis/redis/v7"
	"gitlab.com/jcorry/grpc-demo/service/photo"
)

func NewAPIServer(r *redis.Client) Server {
	p := photo.NewPhoto(r)

	return Server{
		photo: p,
	}
}

// Server is a struct to hold all of the dependencies for the server, this will be provided to the gRPC server
// as the implementation of the contract described in the proto file
type Server struct {
	photo *photo.Photo
}

// Upload accepts a FileUpload argument, uploads the file to Cloudinary and returns
// an UploadStatus
func (s *Server) Upload(stream PhotoService_UploadServer) error {

	for {
		_, err := stream.Recv()
		if err != nil {
			if err == io.EOF {
				goto END
			}
			return errors.Wrapf(err, "failed while reading stream chunks")
		}
	}

END:

	err := stream.SendAndClose(&UploadStatus{
		Message: "Upload received",
		Code:    UploadStatusCode_Ok,
	})

	if err != nil {
		return errors.Wrapf(err, "failed to send upload status")
	}

	return nil
}

// Read accepts a FileIdentifier and uses it to look up a file at Cloudinary
// returning the photo found
func (s *Server) Read(ctx context.Context, i *FileIdentifier) (*Photo, error) {
	return &Photo{}, nil
}

// List returns a list of files that have been uploaded to the service and been stored
// at Cloudinary
func (s *Server) List(ctx context.Context, r *FileListRequest) (*FileIdentifierList, error) {
	return &FileIdentifierList{}, nil
}
