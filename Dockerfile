FROM golang:1.12.0-alpine
# Get OS/build dependencies for cgo
RUN apk --update upgrade
RUN apk add bash
RUN apk add make
RUN apk add git mercurial

# Set workdir
WORKDIR /app

# Remove apk cache
RUN rm -rf /var/cache/apk/*
# env vars
ENV GO111MODULE=auto
ENV CGO_ENABLED=0

## Add the wait script to the image
ADD https://github.com/ufoscout/docker-compose-wait/releases/download/2.6.0/wait /wait
RUN chmod +x /wait

# Move source files
COPY . .

RUN bash -c "go install github.com/golang/protobuf/protoc-gen-go"

RUN bash -c "ls -la /usr/local/go/bin"

RUN make dep

# Run the tests
RUN go test ./...

# Build the app
RUN go build -v -mod=vendor -o ./api ./service

# Echo env vars
RUN bash -c "env"

CMD /wait && /app/api --redis-host=${REDIS_HOST} --cloudinary-api-key=${CLOUDINARY_API_KEY} --cloudinary-api-secret=${CLOUDINARY_API_SECRET} --cloudinary-cloud-name=${CLOUDINARY_CLOUD_NAME}
